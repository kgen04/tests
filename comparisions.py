import statistics
# Function to generate all possibilities of two cards as list of lists containing cards in range 1-52
def twocard_possibilities():
    output_twocards = []
    for j in range(51):
        for k in range(51):
            if j != k:
                output_twocards.append([j+1, k+1, 0])
    return output_twocards


def compare_cards(twocards, drawdata, step=2, gap=0):
    for j in range(gap, len(drawdata)-1, step):
        # Check data exists in both slots
        if drawdata[j] and drawdata[j + 1]:
            # Variable for next 2 cards
            next_two = [drawdata[j], drawdata[j + 1]]
            for poss in twocards:
                card1,card2,count = poss
                if [card1,card2] == next_two:
                    #print(card1,card2,next_two,poss)
                    # Create number of occurences field
                    poss[2] += 1
                    # Unique occurences expected
                    break


def twocards_highprob(twocards):
    # High probability array
    twocards_high = []

    # Array of totals
    twocards_totals = []

    # Collect array of only totals for stastical purposes
    for j, k, l in twocards:
        twocards_totals += [l]

    # DEBUG
    print("Length",len(twocards_totals))
    print("Min",min(twocards_totals), "Max", max(twocards_totals), "Mean", statistics.mean(twocards_totals),
          "Quantiles",statistics.quantiles(twocards_totals))

    # For card1,card2,# of occurrences in given array
    for i, j, k in twocards:
        # If k in highest 23/5 portion
        if k > max(twocards_totals)-max(twocards_totals)/23*5:
            # Add it to our highest probability list
            twocards_high.append([i, j])

    # Return highest probability list
    return twocards_high


