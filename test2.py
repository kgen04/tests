# Import std statistics etc
import statistics

# Import local sqlite manipulations pulldata.py
import pulldata

# Import local card comparisions comparisions.py
import comparisions


# Pull data from all files in target dir (./logfiles)
data = pulldata.dump_data('./logfiles/', 'SELECT BoardCard_1,BoardCard_2,BoardCard_3,BoardCard_4,BoardCard_5 FROM Hand')

# Print total num of elements
print(len(data))
# Print any modes and 25% quantiles
print(statistics.multimode(data))
print(statistics.quantiles(data))

# Given i.e. ababcbaba is number of occurances of ab and ba same/similar? Expecting yes - similar due to odd/even data length?, here we try to test

# Blank array of all possible two card combos + variable for count of each and for n+1 offset
tc = comparisions.twocard_possibilities()
tc_offset = comparisions.twocard_possibilities()

# Confirm empty array
print("Before", tc)
comparisions.compare_cards(tc, data, 2, 0)
print("After", tc)

print("Before", tc_offset)
comparisions.compare_cards(tc_offset, data, 2, 1)
print("After", tc_offset)

# Make arrays of high prob combos
tc_hp = comparisions.twocards_highprob(tc)
tc_hp_offset = comparisions.twocards_highprob(tc_offset)

# Empty list to combine both combos into
tc_hp_both = []

# DEBUG OUTPUT
print(tc_hp)
print(tc_hp_offset)

# If our list not empty - not working or with None? TODO
if tc_hp is not []:
    # For each combo TODO outer loop needs to be biggest, inner smallest
    for item in tc_hp:
        # Split item for thought
        i, j = item
        # For each combo in offset n+1
        for item2 in tc_hp_offset:
            # Flip it backwards/transpose ab = ba
            k, l = item2
            # If it match its backwards pair append it to the both list
            if (i, j) == (l, k):
                tc_hp_both.append(item)
                # TODO why/where are break to?
                break

# Print any matches (should be if actually there?)
print(tc_hp_both)










