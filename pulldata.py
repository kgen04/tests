import os
from os.path import isfile, join

import sqlite3
import csv

# Query existing connection
def query_data(query, conn, output_file=None):
    collected_rows = []
    # New cursor in connection
    cursor = conn.cursor()

    # Select rows in hands table
    for row in cursor.execute(query):
        # Empty array for valid entries
        output_row = []
        # Only append if not null entries
        for i in row:
            if i:
                output_row.append(i)
        # Only append if not null array
        if output_row:
            # Append to csv if present
            if output_file:
                output_file.writerow(output_row)
            # Else to returned array
            else:
                collected_rows += output_row
    # Return
    if output_file:
        return 0
    else:
        return collected_rows


# Dump data matching given query from given dir
def dump_data(logdir, query, output=None):
    # Collect file names from dir
    onlyfiles = [f for f in os.listdir(logdir) if isfile(join(logdir, f))]
    # print("Files", onlyfiles)

    # Empty array for data
    collected_data = []

    # For files in dir
    for file in onlyfiles:
        # print(file)
        # If CSV open file, pass file and query data -> write rows
        if output:
            with open(output, 'w', newline='') as csvfile:
                output_file = csv.writer(csvfile, delimiter=',')
                query_data(query, sqlite3.connect(logdir + file), output_file)
        # Else append to output array
        else:
            collected_data += query_data(query, sqlite3.connect(logdir + file))

    # Else return data
    if not output:
        return collected_data

    # Or give status
    else:
        return 0


#dump_data('./logfiles/', 'SELECT BoardCard_1,BoardCard_2,BoardCard_3,BoardCard_4,BoardCard_5 FROM Hand','output2.csv')

#data = [1,3,1,4]
